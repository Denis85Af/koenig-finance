﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class PaymentSystemsExchangeViewModel
    {
        public string ExchangerName { get; set; }
        public double GetRate { get; set; }
        public double GiveRate { get; set; }
    }
}