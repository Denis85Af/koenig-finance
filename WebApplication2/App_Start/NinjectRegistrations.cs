﻿using Ninject.Modules;
using System.Web.Mvc;
using WebApplication2.Interfaces;
using WebApplication2.Repositories;

namespace WebApplication2.App_Start
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IRateRepository>().To<RateRepository>();
            Unbind<ModelValidatorProvider>();
        }
    }
}