﻿using System.Linq;
using WebApplication2.Dtos;

namespace WebApplication2.Interfaces
{
    public interface IRateRepository
    {
        IQueryable<PaymentSystemDto> GetPaymentSystems();
        IQueryable<PaymentSystemsExchangeDto> GetPaymentSystemsExchanges();
    }
}
