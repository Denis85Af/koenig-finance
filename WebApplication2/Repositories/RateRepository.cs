﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Dal;
using WebApplication2.Dtos;
using WebApplication2.Interfaces;

namespace WebApplication2.Repositories
{
    public class RateRepository : IRateRepository
    {
        private RateContext _context;
        public RateRepository(RateContext context)
        {
            _context = context;
        }

        public IQueryable<PaymentSystemDto> GetPaymentSystems()
        {

            return _context.PaymentSystems
                .Select(p => new PaymentSystemDto
                {
                    Id = p.Id,
                    CodeName = p.CodeName,
                    Name = p.Name
                });
        }

        public IQueryable<PaymentSystemsExchangeDto> GetPaymentSystemsExchanges()
        {
            return _context.PaymentSystemsExchanges               
                .Select(p => new PaymentSystemsExchangeDto
                {
                    ExchangerName = p.Exchanger.Name,
                    SourcePaymentSystemId = p.SourcePaymentSystemId,
                    TargetPaymentSystemId = p.TargetPaymentSystemId,
                    GetRate = p.GetRate,
                    GiveRate = p.GiveRate
                });
        }
    }
}