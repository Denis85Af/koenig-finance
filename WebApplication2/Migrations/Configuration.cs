namespace WebApplication2.Migrations
{
    using System.Data.Entity.Migrations;
    using WebApplication2.Dal;

    internal sealed class Configuration : DbMigrationsConfiguration<RateContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RateContext context)
        {
        }
    }
}
