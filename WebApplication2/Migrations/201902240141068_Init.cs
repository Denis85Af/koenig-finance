namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exchangers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentSystems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CodeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentSystemsExchanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExchangerId = c.Int(nullable: false),
                        SourcePaymentSystemId = c.Int(nullable: false),
                        TargetPaymentSystemId = c.Int(nullable: false),
                        GetRate = c.Double(nullable: false),
                        GiveRate = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exchangers", t => t.ExchangerId)
                .ForeignKey("dbo.PaymentSystems", t => t.SourcePaymentSystemId)
                .ForeignKey("dbo.PaymentSystems", t => t.TargetPaymentSystemId)
                .Index(t => t.ExchangerId)
                .Index(t => t.SourcePaymentSystemId)
                .Index(t => t.TargetPaymentSystemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PaymentSystemsExchanges", "TargetPaymentSystemId", "dbo.PaymentSystems");
            DropForeignKey("dbo.PaymentSystemsExchanges", "SourcePaymentSystemId", "dbo.PaymentSystems");
            DropForeignKey("dbo.PaymentSystemsExchanges", "ExchangerId", "dbo.Exchangers");
            DropIndex("dbo.PaymentSystemsExchanges", new[] { "TargetPaymentSystemId" });
            DropIndex("dbo.PaymentSystemsExchanges", new[] { "SourcePaymentSystemId" });
            DropIndex("dbo.PaymentSystemsExchanges", new[] { "ExchangerId" });
            DropTable("dbo.PaymentSystemsExchanges");
            DropTable("dbo.PaymentSystems");
            DropTable("dbo.Exchangers");
        }
    }
}
