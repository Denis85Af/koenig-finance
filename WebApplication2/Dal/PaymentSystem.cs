﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Dal
{
    public class PaymentSystem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
    }
}