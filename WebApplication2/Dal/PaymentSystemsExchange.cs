﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Dal
{
    public class PaymentSystemsExchange
    {
        public int Id { get; set; }
        public int ExchangerId { get; set; }
        public int SourcePaymentSystemId { get; set; }
        public int TargetPaymentSystemId { get; set; }
        public double GetRate { get; set; }
        public double GiveRate { get; set; }

        public virtual Exchanger Exchanger { get; set; }
        public virtual PaymentSystem SourcePaymentSystem { get; set; }
        public virtual PaymentSystem TargetPaymentSystem { get; set; }
    }
}