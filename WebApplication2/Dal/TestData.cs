﻿using AutoFixture;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WebApplication2.Dal
{
    public class TestData : CreateDatabaseIfNotExists<RateContext>
    {
        protected override void Seed(RateContext context)
        {
            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    if (!context.Exchangers.Any())
                    {
                        Fixture fixture = new Fixture();
                        int i = 1;
                        var exchangers = fixture.Build<Exchanger>()
                            .Without(p => p.Id)
                            .Without(p => p.Name)
                            .Do(p => p.Name = "Exchanger" + (i++.ToString()))
                            .CreateMany(10);
                        context.Exchangers.AddRange(exchangers);
                        context.SaveChanges();

                        var paymentSystems = new List<PaymentSystem>
                        {
                            new PaymentSystem {Name  = "Webmoney WMZ", CodeName = "WebmoneyWMZ"},
                            new PaymentSystem {Name  = "Perfect Money USD", CodeName = "PerfectMoneyUSD"},
                            new PaymentSystem {Name  = "PayPal USD", CodeName = "PayPalUSD"},
                            new PaymentSystem {Name  = "Payza USD", CodeName = "PayzaUSD"},
                            new PaymentSystem {Name  = "Yandex RUB", CodeName = "YandexRUB"}
                        };
                        context.PaymentSystems.AddRange(paymentSystems);
                        context.SaveChanges();

                        foreach (var exchanger in exchangers)
                        {
                            foreach (var sourcePaymentSystem in paymentSystems)
                            {
                                foreach (var targetPymentSystem in paymentSystems)
                                {
                                    var paymentSystemsExchange = fixture.Build<PaymentSystemsExchange>()
                                        .Without(p => p.Id)
                                        .With(p => p.ExchangerId, exchanger.Id)
                                        .With(p => p.SourcePaymentSystemId, sourcePaymentSystem.Id)
                                        .With(p => p.TargetPaymentSystemId, targetPymentSystem.Id)
                                        .Without(p => p.Exchanger)
                                        .Without(p => p.SourcePaymentSystem)
                                        .Without(p => p.TargetPaymentSystem)
                                        .Create();
                                    context.PaymentSystemsExchanges.Add(paymentSystemsExchange);
                                }
                            }
                        }
                        context.SaveChanges();

                        dbContextTransaction.Commit();
                    }
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }
        }
    }
}