﻿using System.Data.Entity;

namespace WebApplication2.Dal
{
    public class RateContext : DbContext
    {

        public RateContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new TestData());
        }

        public DbSet<Exchanger> Exchangers { get; set; }
        public DbSet<PaymentSystem> PaymentSystems { get; set; }
        public DbSet<PaymentSystemsExchange> PaymentSystemsExchanges { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PaymentSystemsExchange>()
                .HasRequired(c => c.Exchanger)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentSystemsExchange>()
               .HasRequired(c => c.SourcePaymentSystem)
               .WithMany()
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentSystemsExchange>()
               .HasRequired(c => c.TargetPaymentSystem)
               .WithMany()
               .WillCascadeOnDelete(false);
        }
    }
}