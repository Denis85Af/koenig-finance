﻿namespace WebApplication2.Dtos
{
    public class PaymentSystemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
    }
}