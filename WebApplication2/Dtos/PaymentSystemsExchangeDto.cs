﻿namespace WebApplication2.Dtos
{
    public class PaymentSystemsExchangeDto
    {
        public string ExchangerName { get; set; }
        public double GetRate { get; set; }
        public double GiveRate { get; set; }
        public int SourcePaymentSystemId { get; set; }
        public int TargetPaymentSystemId { get; set; }
    }
}