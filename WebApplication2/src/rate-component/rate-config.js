﻿var rateApp = angular.module('RateApp', ["ngRoute"]);
rateApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/Home/Rate/:param1/:param2', {
        controller: 'RateController'
    });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
})
    .component('rateComponent', {
        templateUrl: '/src/rate-component/rate-component.html',
        controller: 'RateController'
    })
    .service('RateService', function ($http) {
        this.getPaymentSystems = function () {
            return $http.get('/Home/GetPaymentSystems');
        };
        this.getPaymentSystemsExchanges = function (sourcePaymentExchangeId, targetPaymentExchangeId) {
            return $http.get('/Home/GetPaymentSystemsExchanges?sourcePaymentExchangeId=' + sourcePaymentExchangeId +
                '&targetPaymentExchangeId=' + targetPaymentExchangeId);
        };
    })
    .controller('RateController', function ($scope, $routeParams, RateService) {
        $scope.paymentsSystems = {};
        $scope.soursePaymentSystem = {};
        $scope.targetPaymentSystem = {};
        getPaymentSystems();
        function getPaymentSystems() {
            RateService.getPaymentSystems()
                .then(function (paymentsSystems) {
                    $scope.paymentsSystems = paymentsSystems.data;
                    selectPaymentSystemsFromParams();
                }, function (error) {
                    $scope.status = 'Unable to load customer data: ' + error.message;
                    console.log($scope.status);
                });
        }
        function getPaymentSystemsExchanges() {
            if ($scope.soursePaymentSystem.Id !== undefined && $scope.targetPaymentSystem.Id !== undefined)
                RateService.getPaymentSystemsExchanges($scope.soursePaymentSystem.Id, $scope.targetPaymentSystem.Id)
                    .then(function (exchangers) {
                        $scope.exchangers = exchangers.data;
                        var newUrl = '/Home/Rate/' + $scope.soursePaymentSystem.CodeName
                            + '/' + $scope.targetPaymentSystem.CodeName;
                        window.history.pushState({}, '', newUrl);
                    }, function (error) {
                        $scope.status = 'Unable to load customer data: ' + error.message;
                        console.log($scope.status);
                    });
        }

        $scope.setSourcePaymentSystem = function (soursePaymentSystem) {
            $scope.soursePaymentSystem = soursePaymentSystem;
            getPaymentSystemsExchanges();
        };
        $scope.setTargetPaymentSystem = function (targetPaymentSystem) {
            $scope.targetPaymentSystem = targetPaymentSystem;
            getPaymentSystemsExchanges();
        };

        function selectPaymentSystemsFromParams() {
            var soursePaymentSystemFromParams = $scope.paymentsSystems.find(p => p.CodeName === $routeParams.param1);
            var targetPaymentSystemFromParams = $scope.paymentsSystems.find(p => p.CodeName === $routeParams.param2);
            if (soursePaymentSystemFromParams !== undefined && targetPaymentSystemFromParams !== undefined) {
                $scope.soursePaymentSystem = soursePaymentSystemFromParams;
                $scope.targetPaymentSystem = targetPaymentSystemFromParams;
                getPaymentSystemsExchanges();
            }
            else {
                window.history.pushState({}, '', '/Home/Rate/');
            }
        }
    });