﻿using WebApplication2.Models;
using System.Linq;
using System.Web.Mvc;
using WebApplication2.Repositories;
using WebApplication2.Interfaces;
using System.Web.UI;
using System;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        IRateRepository _repository;
        public HomeController(RateRepository repository)
        {
            _repository = repository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Rate()
        {
            return View();
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetPaymentSystems()
        {
            var paymentSystems = _repository.GetPaymentSystems()
                .Select(p => new PaymentSystemViewModel
                {
                    Id = p.Id,
                    CodeName = p.CodeName,
                    Name = p.Name
                });
            return Json(paymentSystems, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetPaymentSystemsExchanges(int sourcePaymentExchangeId, int targetPaymentExchangeId)
        {
            var exchangers = _repository.GetPaymentSystemsExchanges()
                 .Where(p => p.SourcePaymentSystemId == sourcePaymentExchangeId
                         && p.TargetPaymentSystemId == targetPaymentExchangeId)
                .Select(p => new PaymentSystemsExchangeViewModel
                {
                    ExchangerName = p.ExchangerName,
                    GetRate = p.GetRate,
                    GiveRate = p.GiveRate
                });
            return Json(exchangers, JsonRequestBehavior.AllowGet);
        }
    }
}